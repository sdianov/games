﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessBoard
{

    class Board
    {

        delegate List<Position> Finder(Piece piece, Board board);

        public Action<List<Position>> OnBoardUpdate { get; set; }

        IDictionary<PieceKind, Finder> validators = new Dictionary<PieceKind, Finder>
        {
            { PieceKind.Pawn, ( piece, board) => {
                var res  = new List<Position>();
                int inc = piece.Color == PieceColor.White ? 1 : -1;
                var p1 = new Position(piece.Position.Row + inc, piece.Position.Column);
                if (board.GetAtPosition(p1) == null)
                {
                    res.Add(p1);
                }
                var p2 = new Position(piece.Position.Row + inc + inc, piece.Position.Column);
                if (!piece.Moved && board.GetAtPosition(p2) == null)
                {
                    res.Add(p2);
                }
                var p3 = new Position(piece.Position.Row + inc, piece.Position.Column -1);
                if (board.GetAtPosition(p3) != null)
                {
                    res.Add(p3);
                }
                var p4 = new Position(piece.Position.Row + inc, piece.Position.Column +1);
                if (board.GetAtPosition(p4) != null)
                {
                    res.Add(p4);
                }

                return res;
            } },
            { PieceKind.Knight, ( piece, board) => {
                var res  = new List<Position>();
                var diffs = new List<(int, int)>{
                    (-2,-1), (-2,1), (1, -2), (1, 2), (-1,-2), (-1, 2), (2, -1), (2,1)
                } ;
                return Iterate(piece, board, diffs, true);

                //foreach(var diff in diffs)
                //{
                //    var p = new Position(piece.Position.Row + diff.Item1, piece.Position.Column + diff.Item2);
                //    var other = board.GetAtPosition(p);
                //    if (other == null || other.Color != piece.Color)
                //    {
                //        res.Add(p);
                //    }
                //}
                //return res;
            } },
            { PieceKind.Bishop, ( piece, board) => {
                var diffs = new List<(int, int)>{
                    (- 1,-1), (-1,1), (1, -1), (1, 1)
                } ;
                return Iterate(piece, board, diffs, false);
            } },
            { PieceKind.Rook, ( piece, board) => {
                var diffs = new List<(int, int)>{
                    (0,-1), (0,1), (1, 0), (-1, 0)
                } ;
                return Iterate(piece, board, diffs, false);
            } },
            { PieceKind.Queen, ( piece, board) => {
                var diffs = new List<(int, int)>{
                    (0,-1), (0,1), (1, 0), (-1, 0),  (- 1,-1), (-1,1), (1, -1), (1, 1)
                } ;
                return Iterate(piece, board, diffs, false);
            } },
            { PieceKind.King, ( piece, board) => {
                var diffs = new List<(int, int)>{
                    (0,-1), (0,1), (1, 0), (-1, 0),  (- 1,-1), (-1,1), (1, -1), (1, 1)
                } ;

                 return Iterate(piece, board, diffs, true);
            } }

        };

        private static List<Position> Iterate(Piece piece, Board board, List<(int, int)> diffs, bool limit)
        {
            var res = new List<Position>();
            foreach (var diff in diffs)
            {
                Position p = new Position(piece.Position);
                bool fin = false;
                while (!fin)
                {
                    p = new Position(p.Row + diff.Item1, p.Column + diff.Item2);
                    if (!p.IsValid())
                    {
                        fin = true;
                    }
                    else
                    {
                        var other = board.GetAtPosition(p);
                        if (other == null || other.Color != piece.Color)
                        {
                            res.Add(p);
                        }
                        fin = other != null || limit;
                    }
                }
            }
            return res;
        }


        IList<Piece> Pieces = new List<Piece>();

        public Piece Selected { get; set; }
        public PieceColor NextStep { get; set; } = PieceColor.White;

        private IList<Position> AllowedSteps(Piece piece)
        {
            return null;
        }

        private void ChangeNext()
        {
            NextStep = (NextStep == PieceColor.White) ? PieceColor.Black : PieceColor.White;
        }

        public Piece GetAtPosition(Position position)
        {
            foreach (var piece in Pieces)
            {
                if ((piece.Position.Row == position.Row) && (piece.Position.Column == position.Column))
                {
                    return piece;
                }
            }
            return null;

        }

        public Boolean Select(Position position)
        {
            var piece = GetAtPosition(position);

            if (Selected == null && piece != null)
            {
                if (CanSelect(piece))
                {
                    Selected = piece;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            if (Selected != null)
            {
                if (StepAllowed(Selected, position))
                {
                    Selected.Position = position;
                    if (piece != null)
                    {
                        Pieces.Remove(piece);
                    }
                    Selected.Moved = true;
                    Selected = null;
                    ChangeNext();
                    return true;
                }
            }

            return false;
        }

        private bool CanSelect(Piece piece)
        {
            return piece.Color == NextStep;
        }

        private bool StepAllowed(Piece piece, Position position)
        {
            var validator = validators[piece.Kind];
            var allowed = validator.Invoke(piece, this);
            return allowed.Contains(position);
        }

        public static readonly Board Initial = new Board
        {
            Pieces = new List<Piece>
            {
                new Piece(PieceKind.Pawn, PieceColor.White, 2,  1 ),
                new Piece(PieceKind.Pawn,   PieceColor.White,  2,   2 ),
                new Piece(PieceKind.Pawn,   PieceColor.White,  2,   3 ),
                new Piece(PieceKind.Pawn,   PieceColor.White,  2,   4 ),
                new Piece(PieceKind.Pawn,   PieceColor.White,  2,   5 ),
                new Piece(PieceKind.Pawn,   PieceColor.White,  2,   6 ),
                new Piece(PieceKind.Pawn,   PieceColor.White,  2,   7 ),
                new Piece(PieceKind.Pawn,   PieceColor.White,  2,   8 ),

                new Piece(PieceKind.Rook,   PieceColor.White,  1,   1 ),
                new Piece(PieceKind.Knight,   PieceColor.White,  1,   2 ),
                new Piece(PieceKind.Bishop,   PieceColor.White,  1,   3 ),
                new Piece(PieceKind.Queen,   PieceColor.White,  1,   4 ),
                new Piece(PieceKind.King,   PieceColor.White,  1,   5 ),
                new Piece(PieceKind.Bishop,   PieceColor.White,  1,   6 ),
                new Piece(PieceKind.Knight,   PieceColor.White,  1,   7 ),
                new Piece(PieceKind.Rook,   PieceColor.White,  1,   8 ),

                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   1 ),
                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   2 ),
                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   3 ),
                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   4 ),
                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   5 ),
                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   6 ),
                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   7 ),
                new Piece(PieceKind.Pawn,   PieceColor.Black,  7,   8 ),

                new Piece(PieceKind.Rook,   PieceColor.Black,  8,   1 ),
                new Piece(PieceKind.Knight,   PieceColor.Black,  8,   2 ),
                new Piece(PieceKind.Bishop,   PieceColor.Black,  8,   3 ),
                new Piece(PieceKind.Queen,   PieceColor.Black,  8,   4 ),
                new Piece(PieceKind.King,   PieceColor.Black,  8,   5 ),
                new Piece(PieceKind.Bishop,   PieceColor.Black,  8,   6 ),
                new Piece(PieceKind.Knight,   PieceColor.Black,  8,   7 ),
                new Piece(PieceKind.Rook,   PieceColor.Black,  8,   8 ),


            }
        };
    }
}

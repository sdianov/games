﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChessBoard
{
    public partial class BoardControl : UserControl
    {

        public Action<Position> OnPositionSelected { get; set; }

        public BoardControl()
        {
            InitializeComponent();
            this.MouseClick += new MouseEventHandler(this.OnMouseClick);
        }

        private void OnMouseClick(object sender, MouseEventArgs e)
        {

            if (OnPositionSelected != null)
            {
                OnPositionSelected.Invoke(new Position());
            }
        }

        private Rectangle FromPosition(Position position)
        {
            return new Rectangle
            { 
                X = 0,
                Y = 0,
                Width = 0,
                Height =0 
            };
        }

       

        public void DrawPiece(Position position, Piece piece)
        {
            var rectangle = FromPosition(position);

            var imgIndex = (int)piece.Kind + (int)piece.Color;
            var img = imageList.Images[imgIndex];

        }
    }
}

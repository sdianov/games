﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChessBoard
{
    public partial class MainForm : Form
    {

        private Board MainBoard { get; set; }

        public MainForm()
        {
            InitializeComponent();
            MainBoard = Board.Initial;

            MainBoard.OnBoardUpdate = BoardUpdate;
        }

        private void BoardUpdate(List<Position> positions)
        {
            foreach (var position in positions)
            {

            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            boardPanel.Top = 0;
            boardPanel.Height = this.Height;
            boardPanel.Width = this.Height;
            boardPanel.Left = (this.Width - boardPanel.Width) / 2;

        }

        Point? GetRowColIndex(TableLayoutPanel tlp, Point point)
        {
            if (point.X > tlp.Width || point.Y > tlp.Height)
                return null;

            int w = tlp.Width;
            int h = tlp.Height;
            int[] widths = tlp.GetColumnWidths();

            int i;
            for (i = widths.Length - 1; i >= 0 && point.X < w; i--)
                w -= widths[i];
            int col = i + 1;

            int[] heights = tlp.GetRowHeights();
            for (i = heights.Length - 1; i >= 0 && point.Y < h; i--)
                h -= heights[i];

            int row = i + 1;

            return new Point(col, row);
        }

        Func<int, int, Position> BoardPosition = (screenRow, screenCol) 
            => new Position { Row = 8 - screenRow, Column = screenCol + 1 };

        private void boardPanel_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            Position position = BoardPosition(e.Row, e.Column);

            bool white = (e.Column + e.Row) % 2 == 1;
            e.Graphics.FillRectangle(white ? Brushes.Silver : Brushes.Gray, e.CellBounds);

            var piece = MainBoard.GetAtPosition(position);
            if (piece != null)
            {
                var imgIndex = (int)piece.Kind + (int)piece.Color;
                var img = imageList.Images[imgIndex];

                //if (piece.Color == Board.PieceColor.Black)
               //// {
               //     img.RotateFlip(RotateFlipType.Rotate180FlipY);
               // }

                e.Graphics.DrawImage(img, e.CellBounds);

                if (MainBoard.Selected != null && 
                    piece.Position.Equals(MainBoard.Selected.Position))
                {
                    e.Graphics.DrawRectangle(Pens.Red, e.CellBounds);
                }
            }
        }

        private void UpdateStats()
        {
            nextStepLabel.Text = MainBoard.NextStep.ToString();
        }

        private void boardPanel_MouseClick(object sender, MouseEventArgs e)
        {
            var cellPos = GetRowColIndex(boardPanel, boardPanel.PointToClient(Cursor.Position));
            if (cellPos.HasValue)
            {
                if (MainBoard.Select(BoardPosition(cellPos.Value.Y,cellPos.Value.X)))
                {
                    boardPanel.Invalidate();
                    UpdateStats();
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessBoard
{
    public enum PieceKind
    {
        Pawn = 0,
        Knight = 1,
        Bishop = 2,
        Rook = 3,
        Queen = 4,
        King = 5
    }

    public class Position
    {
        public Position()
        {
            //
        }

        public Position(int row, int col)
        {
            Row = row;
            Column = col;
        }

        public Position(Position original)
        {
            Row = original.Row;
            Column = original.Column;
        }

        public int Row { get; set; }
        public int Column { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   Row == position.Row &&
                   Column == position.Column;
        }

        public override int GetHashCode()
        {
            int hashCode = 240067226;
            hashCode = hashCode * -1521134295 + Row.GetHashCode();
            hashCode = hashCode * -1521134295 + Column.GetHashCode();
            return hashCode;
        }

        public bool IsValid()
        {
            return Row >= 1 && Row <= 8 && Column >= 1 && Column <= 8;
        }

    }

    public enum PieceColor
    {
        White = 6,
        Black = 0
    }

    public class Piece
    {

        public Piece(PieceKind _kind, PieceColor _color, int row, int column)
        {
            Kind = _kind;
            Color = _color;
            Position = new Position { Row = row, Column = column };
        }

        public PieceKind Kind { get; set; }
        public PieceColor Color { get; set; }

        public Position Position { get; set; }

        public bool Moved { get; set; } = false;
    }

}

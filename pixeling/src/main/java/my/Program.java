package my;

import org.eclipse.jetty.server.Server;

class Program {

    public static void main(String[] args) throws Exception {

        Config config = new Config();
        config.init();

        Server server = new Server(8080);

        server.setHandler(new ConvertorHandler());
        server.start();
        //server.dumpStdErr();
        server.join();

    }
}
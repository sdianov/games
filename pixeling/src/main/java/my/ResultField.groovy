package my

import groovy.transform.CompileStatic

@CompileStatic
class ResultField {

    String render(boolean[][] map, boolean toExport) {

        StringBuilder sb = new StringBuilder()
        if (map.length > 0) {

            for (int i : 0..map.length - 1) {
                for (int j : 0..map[i].length - 1) {
                    if (toExport) {
                        sb.append(map[i][j] ? "1," : "0,")
                    } else {
                        sb.append(map[i][j] ? "@" : " ")
                    }
                }
                sb.append("\n")
            }
        }
        sb.toString()
    }
}

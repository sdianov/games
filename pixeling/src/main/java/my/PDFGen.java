package my;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.OutputStream;

public class PDFGen {

    public void generate(boolean[][] map, OutputStream outputStream) throws DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, outputStream);


        document.open();
        document.setMargins(10,10,10,10);


        Font font = FontFactory.getFont(FontFactory.COURIER, 8, BaseColor.BLACK);

        int height = map.length;
        if (height != 0) {
            int width = map[0].length;

            PdfPTable table = new PdfPTable(width);

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    PdfPCell cell = new PdfPCell();
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setPhrase(new Phrase(map[i][j] ? "1" : "0", font));
                    table.addCell(cell);
                }
            }

            document.add(table);
        }
        document.close();
    }
}

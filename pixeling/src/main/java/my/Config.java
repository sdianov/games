package my;

import java.util.ResourceBundle;

public class Config {

    private long dx;

    public void init(){

        ResourceBundle properties = ResourceBundle.getBundle("application");

        dx = Long.parseLong(properties.getString("default.dx"));

    }

}

package my

import groovy.transform.CompileStatic
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@CompileStatic
class ConvertorHandler extends AbstractHandler {

    private final static long DX_DEF = 4
    private final static long DY_DEF = 4
    private final static long TH_DEF = -5884288

    private long getLongParam(HttpServletRequest request, String name, long defaultValue) {
        String paramStr = request.getParameter(name)
        if (paramStr == null) {
            return defaultValue
        }
        return Long.parseLong(paramStr)
    }

    @Override
    void handle(String s, Request request,
                HttpServletRequest httpServletRequest,
                HttpServletResponse httpServletResponse) throws IOException {

        httpServletResponse.setContentType("text/html; charset=utf-8")
        httpServletResponse.setStatus(HttpServletResponse.SC_OK)


        long dx = getLongParam(request, "dx", DX_DEF)
        long dy = getLongParam(request, "dy", DY_DEF)
        long th = getLongParam(request, "th", TH_DEF)

        ConvertParameters parameters = new ConvertParameters(
                deltaX: dx, deltaY: dy, bwThreshold: th)

        Page page = new Page()

        String fileName = request.getParameter("file")

        String errMsg = ""
        boolean[][] map = new boolean[0][0]

        if (fileName == null || fileName.isEmpty()) {
            errMsg = "No filename!"
        } else {
            Convertor convertor = new Convertor()

            try {
                map = convertor.convert(fileName, parameters)
            } catch (IOException e) {
                errMsg = e.getMessage()
            }

        }
        if (request.getParameter("export") != null){
            httpServletResponse.setContentType("application/text; charset=utf-8");
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);

            httpServletResponse.setHeader("content-disposition", "attachment; filename=\"export.csv\"")
            PrintWriter out = httpServletResponse.getWriter();

            out.write(new ResultField().render(map, true));
            request.setHandled(true);
            return;
        }

        if (request.getParameter("exportPDF") != null) {
            httpServletResponse.setContentType("application/pdf")
            httpServletResponse.setStatus(HttpServletResponse.SC_OK)

            httpServletResponse.setHeader("content-disposition", "attachment; filename=\"export.pdf\"")

            new PDFGen().generate(map, httpServletResponse.getOutputStream());

            request.setHandled(true)
            return
        }

        PrintWriter out = httpServletResponse.getWriter()
        out.print(page.render(fileName, parameters, map, errMsg))
        request.setHandled(true)
    }
}

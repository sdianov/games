package my

import groovy.transform.CompileStatic

@CompileStatic
class Page {

    String render(String imageUrl, ConvertParameters parameters, boolean[][] map, String errMsg) {

        """
<html>
<head>

</head>
<body>
    <h1>Pixeling</h1>
    <form id="f1" method="GET"> 
        <p><label for="fileEdit" >URL:&nbsp;</label><input name="file" id="fileEdit" value="${imageUrl}" style="width: 800px"></input>
        <p><label for="dxEdit" >dx:&nbsp;</label> <input name="dx" id="dxEdit" value="${parameters.deltaX}" />
         <p><label for="dyEdit" >dy:&nbsp;</label> <input name="dy" id="dyEdit" value="${parameters.deltaY}" />
        <p>th = ${parameters.bwThreshold}</p>
        <input type="submit" name="display" />

    <div style ="color:red;">$errMsg
    </div>
    <div style="border-style: solid;">
        <pre>
${new ResultField().render(map, false)}
</pre>

    </div>
        <input type="submit" name="export" value="export" />
    </form>
</body>
</html>
"""
    }

}

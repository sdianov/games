package my

import groovy.transform.CompileStatic

@CompileStatic
class ConvertParameters {

    long deltaX;

    long deltaY;

    long bwThreshold;

}

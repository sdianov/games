package my;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class Convertor {

    public boolean[][] convert(String imageUrl, ConvertParameters parameters) throws IOException {

        BufferedImage img = ImageIO.read(new URL(imageUrl));

            int rows = (int) (img.getHeight() / parameters.getDeltaY());
            int cols = (int) (img.getWidth() / parameters.getDeltaX());

            boolean[][] result = new boolean[rows][cols];

            for (int j = 0; j < rows; j++) {

                for (int i = 0; i < cols; i++) {
                    long sum = 0;
                    for (int ix = 0; ix < parameters.getDeltaX(); ix++) {
                        for (int iy = 0; iy < parameters.getDeltaY(); iy++) {
                            int px = (int) (i * parameters.getDeltaX() + ix);
                            int py = (int) (j * parameters.getDeltaY() + iy);

                            int pixel = img.getRGB(px, py);
                            sum += pixel;
                        }
                    }
                    sum = sum / (parameters.getDeltaX() * parameters.getDeltaY());

                    result[j][i] = sum < parameters.getBwThreshold();

                }
        }
        return result;
    }
}
